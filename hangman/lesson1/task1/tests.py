from test_helper import run_common_tests, failed, passed, get_answer_placeholders


def test_answer_placeholders():
    placeholders = get_answer_placeholders()
    placeholder = placeholders[0]
    if placeholder == "from random import *":  # TODO: your condition here
        passed()
    else:
        failed("You need to import the module")

    placeholder = placeholders[1]
    if placeholder == "player_score = 0":  # TODO: your condition here
        passed()
    else:
        failed("You need to declare the player_score variable")

    placeholder = placeholders[2]
    if placeholder == "computer_score = 0":  # TODO: your condition here
        passed()
    else:
        failed("You need to declare the computer_score variable")

if __name__ == '__main__':
    run_common_tests()
    test_answer_placeholders()

