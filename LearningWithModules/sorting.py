def bubble_sort(cards):
    swapped = True
    while swapped:
        swapped = False
        for i in range(len(cards) - 1):
            cur = cards[i]
            j = i + 1
            if cards[i] > cards[j]:
                cards[i] = cards[j]
                cards[j] = cur
                swapped = True
    print(cards)
